// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// [1, [2], [[3]], [[[4]]]]

function flatten(elements, depth = 1) {
    // invalid depth then return original array
    if ( depth <= 0) {
        return elements;
    }
    return flattenElement(elements, depth, []);
}

function flattenElement(elements, depth, result) {
    // element not an array and is not sparse then store it
    if ( !Array.isArray(elements) && elements !== undefined) {
        result.push(elements);
    } else {
        for ( let element of elements) {
            // skip if sparse element
            if ( element === undefined) {
                continue;
            }else if ( depth !== 0) {
                result = flattenElement(element, depth - 1, result);
            } else {
                // required depth reached
                result.push(element);
            }
        }
    }
    return result;
}

module.exports = flatten;