// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

function map(elements, cb) {
    // validating passed data
    if ( !Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function') {
        return [];
    }

    const result = [];
    let index = 0;

    for ( let element of elements) {
        result.push(cb(element, index, elements));
        index++;
    }

    return result;
}

module.exports = map;