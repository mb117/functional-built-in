// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.

function find(elements, cb) {
    // validating passed data
    if ( !Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function') {
        return [];
    }

    for ( let index = 0; index < elements.length; index++) {
        if ( cb(elements[index], index, elements) === true) {
            return true;
        }
    }

    return undefined;
}

module.exports = find;