// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each

function each(elements, cb) {
    // validating passed data
    if ( !Array.isArray(elements) || elements.length === 0 || typeof cb !== 'function') {
        return [];
    }

    let index = 0;

    for ( let element of elements) {
        cb(element, index, elements);
        index++;
    }

    return undefined;
}

module.exports = each;